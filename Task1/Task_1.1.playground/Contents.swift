import Foundation

// Arithmetical and Geometric mean of a few numbers
let a1 = 1
let a2 = 2
let a3 = 4

let arithmeticalMean = Double(a1 + a2 + a3) / 3.0
let geometricMean = pow(Double(a1 * a2 * a3), 1.0 / 3.0)

print("Arithmetical Mean:", arithmeticalMean)
print("Geometric Mean:", geometricMean)



// Quadratic equation

var root1: Double
var root2: Double

let a = 1.0
let b = -2.0
let c = -3.0

if a == 0 && b != 0 {
	// linear equasiton
	root1 = -c / b
} else if a == 0 && b == 0 {
	print("Not an equasion!")
} else {
	// D = b^2 − 4ac.
	let D: Double = b * b - 4 * a * c

	if (D > 0) {
		// two roots
		root1 = (-b + sqrt(D)) / (2 * a)
		root2 = (-b - sqrt(D)) / (2 * a)
		print("\(a)*x^2 + \(b)*x + \(c) = 0, root1=\(root1), root2=\(root2)")
	} else if (D == 0) {
		// one root
		root1 = -b / (2 * a)
		print("\(a)*x^2 + \(b)*x + \(c) = 0, root1=\(root1)")
	} else {
		// no roots
		print("\(a)*x^2 + \(b)*x + \(c) = 0 has no roots")
	}
}
