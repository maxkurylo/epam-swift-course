import Foundation

// Реалізувати enum Transmission (варіатор, типтронік, робот, ручна)
enum Transmission: String {
	case variator
	case tiptronic
	case robot
	case manual
}

// Реалізувати структуру - Car з властивістями модель (String), потужність (Int), коробка передач(Transmission).
struct Car {
	let model: String
	let horsePower: Int
	let transmission: Transmission
	
	init(model: String, horsePower: Int, transmission: Transmission) {
		self.model = model
		self.horsePower = horsePower
		self.transmission = transmission
	}
}

// Додати пропертю description за допомогою розширення структури Car протоколом CustomStringConvertible.
extension Car: CustomStringConvertible {
	var description: String {
		"""
		\n
		Model: \(model)
		Horse power: \(horsePower)
		transmission: \(transmission)
		\n
		"""
	}
}

// Реалізувати фукцію яка буде приймати массив з машинами і повертати масив масивів з
// автомобілями ([[Car]]) групованих по типу коробки передач

func groupByTransmissionType(_ cars: [Car]) -> [[Car]] {
	var result: [[Car]] = []
	let groupedByTransmission = Dictionary(grouping: cars) { $0.transmission }
	for (_, groupedCars) in groupedByTransmission {
		result.append(groupedCars)
	}
	return result
}

// Створіть массив з машинами в яких різні коробки передач.
let cars: [Car] = [
	Car(model: "Porsche Cayenne", horsePower: 400, transmission: .tiptronic),
	Car(model: "Chevrolet Aveo", horsePower: 90, transmission: .manual),
	Car(model: "Nissan Pathfinder", horsePower: 170, transmission: .robot),
	Car(model: "Lada Samara", horsePower: 76, transmission: .manual),
	Car(model: "Lada Granta", horsePower: 92, transmission: .robot),
	Car(model: "Peugeot 206", horsePower: 65, transmission: .robot)
]


// Передайте масив в створену Вами функцію і виведіть результат з використанням змінної description
print(groupByTransmissionType(cars))




// Додати екстеншен для Transmission в якому іплементувати протокол Hashable
extension Transmission: Hashable {
	func hash(into hasher: inout Hasher) { // read about it
		hasher.combine(self.rawValue)
	}
}

// Реалізувати в екстеншені массиву функцію group(by: (element: Element) -> AnyHashable) -> [[Element]]
extension Array {
	func group(by criteria: (Element) -> AnyHashable) -> [[Element]] {
		var result: [[Element]] = []
		let groupedByCriteria = Dictionary(grouping: self, by: criteria)
		for (_, groupedElements) in groupedByCriteria {
			result.append(groupedElements)
		}
		return result
	}
}


print("With closures")
print(cars.group(by: { $0.transmission }))
