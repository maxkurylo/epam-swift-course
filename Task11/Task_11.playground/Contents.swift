import Foundation


protocol Product {
	var name: String { get set }
	var color: String { get set }
	var size: String { get set }
	var price: Double { get set }
}

protocol Order {
	var product: Product { get set }
	var amount: Int { get set }
	init(product: Product, amount: Int)
}

protocol Shop {
	func newProductArrived(_ product: Product)
	func getAllProducts() -> [Product]
	func processOrder(order: Order, completion: () -> Void)
}

protocol StorageDelegate {
	func processOrder(order: Order, completion: () -> Void)
	func getAllProducts() -> [Product]
}

protocol Storage: StorageDelegate {
	var products: [Product] { get set }
	func addProduct(_ product: Product)
	func addShop(_ shop: Shop)
	func removeShop(_ shop: Shop)
}



class ClothesShop: Shop {
	weak var storeDelegate: Store?
	
	func newProductArrived(_ product: Product) {
		print("Message from shop: new product arrived!", product.name)
	}
	
	func getAllProducts() -> [Product] {
		return storeDelegate?.getAllProducts() ?? []
	}
	
	func processOrder(order: Order, completion: () -> Void) {
		// trow error if no delegate
		guard let storeDelegate = storeDelegate else {
			print("Cannot create order as no storage delegated")
			return
		}
		storeDelegate.processOrder(order: order, completion: completion)
	}
}



class Store: Storage {
	var products: [Product] = []
	var shops: [Shop] = []
	
	static var shared: Store = { return Store() }()
	
	private init() {}
	
	func addProduct(_ product: Product) {
		products.append(product)
		shops.forEach({ $0.newProductArrived(product) }) // observer
	}
	
	func addShop(_ shop: Shop) {
		shops.append(shop)
	}
	
	func removeShop(_ shop: Shop) {
		for (index, clothesShop) in shops.enumerated() {
			if let clothesShop = clothesShop as? ClothesShop, let shop = shop as? ClothesShop {
				if clothesShop === shop {
					shops.remove(at: index)
					break
				}
			}
		}
	}
	
	func processOrder(order: Order, completion: () -> Void) {
		print("Order on \(order.product.name) on \(order.amount)pcs")
		completion()
	}
	
	func getAllProducts() -> [Product] {
		return products
	}
}


class Clothes: Product {
	var name: String
	var color: String
	var size: String
	var price: Double
	
	init(name: String, color: String, size: String, price: Double) {
		self.name = name
		self.color = color
		self.size = size
		self.price = price
	}
}


class ClothesOrder: Order {
	var product: Product
	var amount: Int
	
	required init(product: Product, amount: Int) {
		self.amount = amount
		self.product = product
	}
}



let store = Store.shared

let shop1 = ClothesShop()
let shop2 = ClothesShop()
let shop3 = ClothesShop()

shop1.storeDelegate = store
shop2.storeDelegate = store
shop3.storeDelegate = store

store.addShop(shop1)
store.addShop(shop2)
store.addShop(shop3)

let product1 = Clothes(name: "Sweater", color: "Red", size: "XL", price: 20.99)
let product2 = Clothes(name: "T-Shirt", color: "Blue", size: "M", price: 10.99)
let product3 = Clothes(name: "Shorts", color: "Yellow", size: "S", price: 5.99)

store.addProduct(product1)
store.addProduct(product2)

store.removeShop(shop1)
store.removeShop(shop3)

store.addProduct(product3)

let order = ClothesOrder(product: product2, amount: 1)
shop3.processOrder(order: order, completion: { print("Order successfully placed!") })

