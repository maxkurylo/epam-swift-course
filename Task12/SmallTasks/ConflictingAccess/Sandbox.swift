//
//  Sandbox2.swift
//  SmallTasks
//
//  Created by Oleksandr Sulyma on 17.07.2021.
//

import Foundation


// copy and run inside online playground


class RunnerForIncrementingWithRuntimeError {
    var  a = 1

    func myPlusEqual(_ parameter: inout Int) {
        parameter += a
    }

    func executeAllWork() {
        print("before: \(a)") // 1
        myPlusEqual(&a) // Error: Simultaneous accesses to 0x7fdf5da38190, but modification requires exclusive access.
        print("after: \(a)")
    }

}


class Foo: NSObject {
    deinit {
        print("⚠️ Hi, I'm \(self.description) and I'm about to die.")
    }
}



class RunnerForIncrementing {
    var  a = 1

    func myPlusEqual(_ parameter: inout Int) {
        parameter += a
    }

    func executeAllWork() {
        var copyA = a
        print("before: \(a)") // 1
        myPlusEqual(&copyA)
        a = copyA
        print("after: \(a)") // 2
    }

}



// uncomment to execute exersise with conflicting access
//let instanceOfCorruptedRunner = RunnerForIncrementingWithRuntimeError().executeAllWork()

// uncomment to execute exersise without conflicting access
//let instanceOfRunner = RunnerForIncrementing().executeAllWork()
