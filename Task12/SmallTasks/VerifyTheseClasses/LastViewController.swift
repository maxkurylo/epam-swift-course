//
//  LastViewController.swift
//  SmallTasks
//
//  Created by Oleksandr Sulyma on 17.07.2021.
//

import UIKit


class StudentViewModel {
    var name: String?
    
    unowned var masterVC: UIViewController
    
    init(viewController: UIViewController) {
        masterVC = viewController
    }
}

class LastViewController: UIViewController {
    
    var model: StudentViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = StudentViewModel(viewController: self)

        // Do any additional setup after loading the view.
    }

}
