//
//  ViewController.swift
//  SmallTasks
//
//  Created by Oleksandr Sulyma on 24.02.2021.
//

import UIKit


class CarModel {
    var vendorName: String?
    var carType: String?
    var price: UInt8?
}

protocol AmazingViewModelDelegate {
    func didPrepareDataToDisplaying()
    func didReceiveFailure()
}

class AmazingViewModel {
    
    let cars: [CarModel]
    private let delegate: AmazingViewModelDelegate
    
    init(models: [CarModel], viewModelDelegate: AmazingViewModelDelegate) {
        cars = models
        delegate = viewModelDelegate
    }
    
    // MARK:  - Interaction with VC
    private func everythingIsGood() {
        delegate.didPrepareDataToDisplaying()
    }
    
    private func everythingIsBad() {
        delegate.didReceiveFailure()
        
//        let starterVC = StarterViewController()
//        starterVC.viewDidLoad()
    }

}

// example #1
class StarterViewController: UIViewController, AmazingViewModelDelegate {
    
    weak var viewModel: AmazingViewModel? // FIX: wasn't weak. There always was a reference to Model

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // run Conflicting Access
        //RunnerForIncrementingWithRuntimeError().doAllWork()
        
        let car1 = CarModel()
        let car2 = CarModel()

        let cars = [car1, car2]
        self.viewModel = AmazingViewModel(models: cars, viewModelDelegate: self)
    }
    
    // MARK:  - AmazingViewModelDelegate
    func didPrepareDataToDisplaying() { }
    func didReceiveFailure() { }
}
