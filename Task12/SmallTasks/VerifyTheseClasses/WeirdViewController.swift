//
//  WeirdViewController.swift
//  SmallTasks
//
//  Created by Oleksandr Sulyma on 05.03.2021.
//

import UIKit


// example #2
class WeirdViewController: UIViewController {
    
	weak var viewModel: WeirdViewModel? // fix for weak
//    private /*weak*/ var helper: Foo?
    // private var helper: Foo? // strong ref.
    //private unowned let helper: Foo

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let instance = Foo()
        //helper = instance

        
		let cars: [CarModel] = []
		
		weak var weakSelf = self // FIX: reference
        self.viewModel = WeirdViewModel(models: cars, completion: { hasNewDataToDisplay in
            if hasNewDataToDisplay {
                // tasks for drawing view model data
                print("WeirdViewController will display view model data")
            } else {
//				Old code: closure holds self reference
//                self.navigationController?.popViewController(animated: true)
				weakSelf?.navigationController?.popViewController(animated: true)
            }

        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        helper?.description
    }
	
	
	deinit {
		print("WeirdViewController deinit")
	}
    
}


typealias ReadynessCallback = ((Bool) -> Void)

class WeirdViewModel {
        
    let cars: [CarModel]
    private let callbackKeeper: ((Bool) -> Void)
    
    init(models: [CarModel], completion: @escaping ((Bool) -> Void)) {
        cars = models
        callbackKeeper = completion
    }
        
    // MARK:  - Interaction with VC
    func parseModel(cars: [CarModel]) {
        // some code
        // .....
        let isReadyToShow: Bool = cars.count > 0
        
        if isReadyToShow {
            callbackKeeper(true)
        }
    }
	
	deinit {
		print ("WEIRD MODEL DEINIT")
	}
}

