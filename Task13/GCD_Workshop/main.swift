//
//  main.swift
//  GCD_Workshop
//
//  Created by Yehor Chernenko on 03.08.2021.
//

import Foundation

/* Sync vs Async
for i in 1...10 {
    DispatchQueue.global().sync {
        //task 1
        print("🟢 \(i) of 10")
    }
}

//task 2
for i in 1...10 {
    print("🔴 \(i) of 10")
}
*/

/* Sync/Async + Serail vs Concurrent
let queue = DispatchQueue.global()
//let queue = DispatchQueue(label: "com.epam.serial")

for i in 0...10 {
    if i == 5 {
        queue.sync {
            sleep(3)
            print("🔴")
        }
    } else {
        queue.async {
            sleep(1)
            print("🟢")
        }
    }
}
*/

/* Quality of service
for _ in 0...10 {
    DispatchQueue.global(qos: .background).async {
        print("🔴")
    }
    
//    DispatchQueue.global(qos: .background).async(qos: .userInteractive, flags: .enforceQoS) {
//        print("🔴 forced")
//    }
    
    DispatchQueue.global(qos: .userInteractive).async {
        print("🟢")
    }
}
*/


// Race condition
//let queue = DispatchQueue(label: "com.epam.race-condition")
//
//var value = "🟠"
//
//queue.async { // use .sync to fix
//    sleep(1)
//    value += "🟣"
//}
//print(value)
//
//value = "🟩"
//queue.sync {
//    sleep(1)
//    value += "🟦"
//}
//print(value)


// Пофіксити RaceCondition з прикладу ішним способом, а саме зробити класс
// ThreadSafeString. Забезпечети потокобезпечність використовуючи private queue та
// dispatch barrier.

var resource = "🔴"

class ThreadSafeString {
	private static let queue = DispatchQueue(label: "com.epam.race-condition", attributes: .concurrent)
	
	func addValueAsyncWithBarier(sleepTime: UInt32 = 0) {
		ThreadSafeString.queue.async(flags: .barrier) {
			if (sleepTime > 0) {
				sleep(sleepTime)
			}
			resource += "🟣"
			print("Async add with \(sleepTime)s delay")
			print(resource)
		}
	}
	
	func addValueAsync(sleepTime: UInt32 = 0) {
		ThreadSafeString.queue.async() {
			if (sleepTime > 0) {
				sleep(sleepTime)
			}
			resource += "🟦"
			print("Async add with \(sleepTime)s delay")
			print(resource)
		}
	}
	
	func addValueSync(sleepTime: UInt32 = 0) {
		ThreadSafeString.queue.sync() {
			if (sleepTime > 0) {
				sleep(sleepTime)
			}
			resource += "🟩"
			print("Sync add with \(sleepTime)s delay")
			print(resource)
		}
	}
}

let myThreadSafe1 = ThreadSafeString()
let myThreadSafe2 = ThreadSafeString()
myThreadSafe1.addValueSync(sleepTime: 2)
myThreadSafe2.addValueAsyncWithBarier(sleepTime: 4)
myThreadSafe1.addValueAsync(sleepTime: 3)
myThreadSafe1.addValueSync(sleepTime: 1)


/* Dispatch Group
let group = DispatchGroup()
workerQueue.async(group: group) {
    sleep(1)
    print("Task 1")
}
mySerialQueue.async(group: group) {
    sleep(1)
    print("Task 2")
}

group.notify(queue: DispatchQueue.main) {
    print("Both tasks have completed")
}
*/




RunLoop.main.run()
