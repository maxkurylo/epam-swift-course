import Foundation

// chance of errors 0.1 ... 0.9
let N = 0.2
let M = 0.9
let L = 0.9
let P = 0.9


enum WeatherError: Error {
	case weatherError(chance: Double)
	case weatherCallbackError(chance: Double)
	case weatherCallbackOptionalError(chance: Double)
}


struct WeatherForecast {
	var temperature: Double
	var humidity: Double
	var chanceOfRain: Double
	
	init(temperature: Double, humidity: Double, chanceOfRain: Double) {
		self.temperature = temperature
		self.humidity = humidity
		self.chanceOfRain = chanceOfRain
	}
}

// Написати клас `WeatherDataProvider`, забезпечити існування тільки одного екземпляру цього класу
class WeatherDataProvider {
	var weatherServiceUrl: URL?
	
	static var shared: WeatherDataProvider = { return WeatherDataProvider() }()
	private init() {}

	
//  конфігурує наш провайдер з урлом на сервіс звідки ми "отримуємо дані"
	func configure(weatherServiceUrl: URL) {
		self.weatherServiceUrl = weatherServiceUrl
	}

//  методи що повертають прогноз погоди на вказану дату, всі ці методи повинні перевірити чи наш
//  провайдер сконфігурований використовуючи `precondition`
	
//  в N відсотках випадків повинен викидати помилку
	func getWeatherThrows(date: Date) throws -> WeatherForecast {
		precondition(weatherServiceUrl != nil, "No weather service url provided")
		if shouldReturnError(chanceOfError: N) {
			throw WeatherError.weatherError(chance: N)
		}
		return WeatherForecast(temperature: 25, humidity: 0.7, chanceOfRain: 0.4)
	}
	
//	в M відсотках випадків повинен повертати `nil`
	func getWeatherOptional(date: Date) -> WeatherForecast? {
		precondition(weatherServiceUrl != nil, "No weather service url provided")
		if shouldReturnError(chanceOfError: M) {
			return nil
		}
		return WeatherForecast(temperature: 30, humidity: 0.5, chanceOfRain: 0.1)
	}
	
//	в L відсотках випадків повинен повертати `failure`
	func getWeatherCallbackResult(date: Date, callback: (Result<WeatherForecast, WeatherError>) -> ()) {
		precondition(weatherServiceUrl != nil, "No weather service url provided")
		if shouldReturnError(chanceOfError: L) {
			callback(.failure(WeatherError.weatherCallbackError(chance: L)))
		} else {
			let forecast = WeatherForecast(temperature: 10, humidity: 0.8, chanceOfRain: 0.9)
			callback(.success(forecast))
		}
	}
	
//	в P відсотках випадків повинен повертати `nil` як прогноз і помилку
	func getWeatherCallbackOptionals(date: Date, callback: (WeatherForecast?, WeatherError?) -> ()) {
		precondition(weatherServiceUrl != nil, "No weather service url provided")
		
		if shouldReturnError(chanceOfError: P) {
			callback(nil, WeatherError.weatherCallbackOptionalError(chance: P))
		} else {
			let forecast = WeatherForecast(temperature: 18, humidity: 0.9, chanceOfRain: 0.0)
			callback(forecast, nil)
		}
	}
}


func shouldReturnError(chanceOfError: Double) -> Bool {
	return Double.random(in: 0...1) < chanceOfError
}



let weather = WeatherDataProvider.shared
if let url = URL(string: "https://google.com") {
	weather.configure(weatherServiceUrl: url)
}
let tomorrow = Date(timeIntervalSinceNow: 24 * 60 * 60 * 1000)



// для функції getWeatherThrows виконати щонайменше 3 виклики:
// 1) `do` `try` `catch`
// 2) `try?`
// 3) `try!`
do {
	let forecast = try weather.getWeatherThrows(date: tomorrow) // tomorrow
	print ("Forecast for tomorrow: \(forecast)")
} catch WeatherError.weatherError(let chance) {
	print("Error on getWeatherThrows with chance \(chance)")
}

if let forecast = try? weather.getWeatherThrows(date: tomorrow) {
	print ("Forecast for tomorrow with try?: \(forecast)")
} else {
	print ("No forecast for getWeatherThrows with try?")
}

var forecast: WeatherForecast = try! weather.getWeatherThrows(date: tomorrow)
	print ("Forecast for tomorrow with try!: \(forecast)")




// calling getWeatherOptional
if let forecast = weather.getWeatherOptional(date: tomorrow) {
	print ("Forecast from getWeatherOptional: \(forecast)")
} else {
	print ("No forecast for getWeatherOptional")
}



weather.getWeatherCallbackResult(date: tomorrow, callback: { result in
	switch result {
		case .success(let forecast):
			print ("Forecast from getWeatherCallbackResult: \(forecast)")
		case .failure(let chance):
			print("Error on getWeatherCallbackResult with chance \(chance)")
		}
})



weather.getWeatherCallbackOptionals(date: tomorrow) { (forecast, error) in
	if let chance = error {
		print("Error on getWeatherCallbackOptionals with chance \(chance)")
		return
	}
	guard let forecast = forecast else {
		print ("No forecast provided in getWeatherOptional")
		return
	}
	print ("Forecast from getWeatherCallbackOptionals: \(forecast)")
}

