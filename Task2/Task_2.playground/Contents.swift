import Foundation

let allStudents: Set = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

let presentOnMonday: Set = [1, 2, 5, 6, 7, 10]
let presentOnTuesday: Set = [3, 6, 8, 10]
let presentOnWednesday: Set = [1, 3, 7, 9, 10]

let presentOnMondayAndWednesday: Set = presentOnMonday.intersection(presentOnWednesday)
let presentThreeDays: Set = presentOnMondayAndWednesday.intersection(presentOnTuesday)
let presentTwoDays: Set = presentOnMondayAndWednesday
	.union(presentOnMonday.intersection(presentOnTuesday))
	.union(presentOnTuesday.intersection(presentOnWednesday))

let missedAllClasses: Set = allStudents
	.subtracting(presentOnMonday)
	.subtracting(presentOnTuesday)
	.subtracting(presentOnWednesday)

print("Present for all tree days: ", presentThreeDays)
print("Present for two days: ", presentTwoDays)
print("Present on Monday and Tuesday: ", presentOnMondayAndWednesday)
print("Missed all classes: ", missedAllClasses)
