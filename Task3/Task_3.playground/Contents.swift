import Foundation

let numbers: [Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

var evenNumbers: [Int] = []
var oddNumbers: [Int] = []

print ("Iterating with for in")
for num in numbers {
	print(num)
	num % 2 == 0 ? evenNumbers.append(num) : oddNumbers.append(num)
}

var whileIndex = 0
print ("Iterating with while")
while whileIndex < numbers.count {
	print(numbers[whileIndex])
	whileIndex += 1
}

var repeatWhileIndex = 0
print ("Iterating with repeat-while")
repeat {
	print(numbers[repeatWhileIndex])
	repeatWhileIndex += 1
}
while repeatWhileIndex < numbers.count


print("Even numbers: ", evenNumbers)
print("Odd numbers: ", oddNumbers)
