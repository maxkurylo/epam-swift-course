import Foundation

// Реалізувати функцію, що розвязує квадратне рівняння і повертає розвязки tuple і error
// enum (якщо дискримінант < 0, значення дискримінанта в помилці як Associated Value)
// (нам знадобиться protocol Error, throw, try);

// ax2 + bx + c

enum equasionError: Error {
	case negativeDiscriminant(_ discriminant: Double)
	case notEquasion
}


func quadraticEquasion(a: Double, b: Double, c: Double) throws -> (Double, Double?) {
	if a == 0 && b != 0 {
		// linear equasiton
		return (-c / b, nil)
	}
	if a == 0 && b == 0 {
		// makes no sense, because it is not even equasion
		throw equasionError.notEquasion
	}
	
	let D: Double = b * b - 4 * a * c
	
	if D > 0 {
		let root1 = (-b + sqrt(D)) / (2 * a)
		let root2 = (-b - sqrt(D)) / (2 * a)
		return (root1, root2)
	} else if D == 0 {
		return (-b / 2 * a, nil)
	} else {
		throw equasionError.negativeDiscriminant(D)
	}
}

do {
	let roots = try quadraticEquasion(a: 2, b: -11, c: 5)
	print("Roots of equasion", roots)
} catch equasionError.notEquasion {
	print("Invalid coefficients")
} catch equasionError.negativeDiscriminant(let discriminant) {
	print("Negative discriminant", discriminant)
}
