import Foundation

let baseNumbers = [1, 2, 12, 5, 7, 88]
let baseStrings = ["1", "2", "12", "bla", "5", "7", "88"]

// 1. З масиву чисел вивести тільки парні (ex: [1, 2, 12, 5, 7, 88])
func even(_ numbers: [Int]) -> [Int] {
	return numbers.filter {$0 % 2 == 0}
}

print(even(baseNumbers))



// 2. Обчислити суму всіх чисел з масиву (ex: [1, 2, 12, 5, 7, 88])
func sum(_ numbers: [Int]) -> Int {
	return numbers.reduce(1, +)
}

print(sum(baseNumbers))


// 3. Конвертувати масив [Strings] у масив [Int] (ex: ["1", "2", "12", "bla", "5", "7", "88"]) -> ([])

func toString(_ array: [String]) -> [Int] {
	return array.compactMap { Int($0) }
}

print(toString(baseStrings))
