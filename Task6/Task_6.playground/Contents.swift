import Foundation


// Реалізувати свій Optional(MyOptional).
// 2. Реалізувати функцію, яка в якості аргументів буде приймати 2 числа типу MyOptional<Int>
// і повертати суму цих чисел також типу MyOptional<Int>.


enum MyOptional<T> : ExpressibleByNilLiteral {
	case none
	case some(T)

	public init(_ value: T) {
		self = .some(value)
	}
	
	public init(nilLiteral: ()) {
		self = .none
	}
}

func +(_ a: MyOptional<Int>, _ b: MyOptional<Int>) -> MyOptional<Int> {
	guard case .some(let aVal) = a, case .some(let bVal) = b else {
		return MyOptional.none
	}
	
	return MyOptional.some(aVal + bVal)
}


let a: MyOptional<Int> = MyOptional(41)
let b: MyOptional<Int> = MyOptional(1)

print(a + b)


