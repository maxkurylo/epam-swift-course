import Foundation


// Використовуючи Enum реалізувати LinkedList
// Методи:
// append(value : T) - додати в кінець
// description - повертає строку зі всіма значеннями починаючи з голови.

indirect enum LinkedListItem<T> {
	case end
	case item(prev: LinkedListItem<T>, value: T, next: LinkedListItem<T>)
}


class LinkedList<T> {
	var head: LinkedListItem<T>
	
	init() {
		head = LinkedListItem.end
	}
	
	
	// goes through list from last item to the start
	func description() -> String {
		var item: LinkedListItem<T> = head
		var result = ""
		
		while true {
			switch item {
			case let .item(_, value, next):
				result += "\(value) "
				item = next
			case .end:
				return result
			}
		}
	}
	
	
	private func addNode(node: LinkedListItem<T>, insertValue: T) -> LinkedListItem<T> {
		switch node {
		case .end:
			return .item(prev: .end , value: insertValue, next: .end)
		case let .item(prev, nodeValue, next):
			switch next {
			case .end:
				let newNode: LinkedListItem<T> = .item(prev: prev, value: insertValue, next: .end)
				return .item(prev: prev, value: nodeValue, next: newNode)
			case .item:
				// go through the list and update link to each node
				let updatedNode = addNode(node: next, insertValue: insertValue)
				return .item(prev: prev, value: nodeValue, next: updatedNode)
			}
				
		}
	}
	
	
	func append(_ value: T) {
		head = addNode(node: head, insertValue: value)
	}
	
}


let list = LinkedList<Int>()

list.append(1)
list.append(2)
list.append(3)


print(list.description())



//indirect enum LinkedList<T> {
//	case start
//	case end
//	case item(prev: LinkedList<T>, value: T, next: LinkedList<T>)
//
//
//	// goes through list from last item to the start
//	func description(_ listItem: LinkedList<T>, _ result: String = "") -> String {
//		switch listItem {
//		case let .item(prev, value, _):
//			return description(prev,  "\(value) " + result)
//		case .start, .end:
//			return result
//		}
//	}
//
//
//	func append(_ value: T) -> LinkedList<T> {
//		switch self {
//		case .start, .end:
//			return .item(prev: .start, value: value, next: .end)
//		case .item:
//			return .item(prev: self, value: value, next: .end) // self does not get updated next
//		}
//	}
//
//	public init() {
//		self = .start
//	}
//}
//
//var list: LinkedList<Int?> = LinkedList<Int?>()
//list = list.append(1)
//list = list.append(2)
//list = list.append(3)
//list = list.append(nil)
//
//print(list.description(list))



