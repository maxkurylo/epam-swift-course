import Foundation

// Побудувати бінарне дерево використовуючи рекурсивне виведення

indirect enum BinaryTree {
	case empty
	case node(left: BinaryTree, value: Int, right: BinaryTree)
	
	
	func getTreeDiagram(node: BinaryTree, _ result: String = "", _ childIdent: String = "") -> String {
		switch node {
		case let .node(left, value, right):
			var newResult = result + "\(value)"
			switch left {
			case .empty:
				break
			case .node:
				newResult += getTreeDiagram(node: left, "\n\(childIdent)l: ", childIdent + "   ")
			}
			
			switch right {
			case .empty:
				break
			case .node:
				newResult += getTreeDiagram(node: right, "\n\(childIdent)r: ", childIdent + "   ")
			}
			
			return newResult
		case .empty:
			return result
		}
	}
	
	
	private func addNode(node: BinaryTree, insertValue: Int) -> BinaryTree {
		switch node {
		case .empty:
			return .empty
		case let .node(left, nodeValue, right):
			if insertValue > nodeValue {
				switch right {
				case .empty:
					let newNode: BinaryTree = .node(left: .empty, value: insertValue, right: .empty)
					return .node(left: left, value: nodeValue, right: newNode)
				case .node:
					let updatedNode = addNode(node: right, insertValue: insertValue)
					return .node(left: left, value: nodeValue, right: updatedNode)
				}
				
			} else {
				switch left {
				case .empty:
					let newNode: BinaryTree = .node(left: .empty, value: insertValue, right: .empty)
					return .node(left: newNode, value: nodeValue, right: right)
				case .node:
					let updatedNode = addNode(node: left, insertValue: insertValue)
					return .node(left: updatedNode, value: nodeValue, right: right)
				}
			}
		}
	}
	
	
	public func insertNode(_ value: Int) -> BinaryTree {
		return self.addNode(node: self, insertValue: value)
	}
	
	
	init(_ value: Int) {
		self = .node(left: .empty, value: value, right: .empty)
	}
}

var tree: BinaryTree = BinaryTree(7)
tree = tree.insertNode(5)
tree = tree.insertNode(8)
tree = tree.insertNode(2)
tree = tree.insertNode(1)
tree = tree.insertNode(4)
tree = tree.insertNode(9)


print(tree.getTreeDiagram(node: tree))

