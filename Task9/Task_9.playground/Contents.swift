import Foundation


// 1. Создать структуру Employee с переменными firstName, lastName тип String.
// Создать структуру Product с переменной name тип String. Добавить если
// необходимо инициализаторы.

struct Employee {
	var firstName: String
	var lastName: String
}

struct Product {
	var name: String
}


// 2. Создать класс Company с переменными:
// - list тип [Employee]
// - name тип String
// Также в класс добавить функцию, которая возвращает Product.
// 3. В классе Company необходимо реализовать 3 инициализатора:
// - required init()
// - с параметрами list : [Employee], name: String, который присваивает значение
// параметров переменным list и name
// - сonvenience failable с параметрами employee : Employee?, name: String,
// который проверяет или параметр employee имеет значение и делегирует
// выполнение предыдущему инициализатору.

class Company {
	var list: [Employee]
	var name: String
	
	required init() {
		list = []
		name = ""
	}
		
	init(list: [Employee], name: String) {
		self.list = list
		self.name = name
	}
	
	convenience init?(employee: Employee?, name: String) {
		guard let employee = employee else {
			return nil
		}
		self.init(list: [employee], name: name)
	}
	
	func getProduct(name: String) -> Product {
		return Product(name: name) // clarify how it should work
	}
}


// 4. Далее необходимо создать класс StateRegistry со static переменной
// registeredCompanies тип [Company], и двумя функциями, первая добавляет
// компанию в registeredCompanies, вторая принимает name как параметр и
// проверяет наличие компании в registeredCompanies.

class StateRegistry {
	static var registeredCompanies: [Company] = []
	
	class func registerCompany(_ company: Company) {
		if !company.name.isEmpty && !StateRegistry.hasCompany(name: company.name) {
			StateRegistry.registeredCompanies.append(company)
		}
	}
	
	class func hasCompany(name: String) -> Bool {
		return StateRegistry.registeredCompanies.contains(where: { $0.name == name })
	}
}

// 5. Также необходимо создать подкласс FoodCompany, с переменной
// qualityCertificate тип String без дефолтного значения, в классе необходимо
// реализовать failable инициализатор с параметрами
// employee : (String?, String?), name: String, qualityCertificate: String, который в
// случае правильных данных(не nil, не пустые строки) делегирует выполнение, наверх.

class FoodCompany: Company {
	var qualityCertificate: String
	
	required init() {
		self.qualityCertificate = ""
		super.init()
	}
	
	init?(employee : (firstName: String?, lastName: String?), name: String, qualityCertificate: String) {
		guard
			let firstName = employee.firstName,
			let lastName = employee.lastName,
			!firstName.isEmpty,
			!lastName.isEmpty,
			!name.isEmpty,
			!qualityCertificate.isEmpty
		else {
			return nil
		}
		self.qualityCertificate = qualityCertificate
		super.init(list: [Employee(firstName: firstName, lastName: lastName)], name: name)
	}
}


// 6. Далее необходимо создать класс Project с переменными
// - contractor тип Company
// - name тип String
// и failable инициализатор с параметрами (name: String, company: Company),
// который проверяет наличие компании в registeredCompanies и в случае успеха
// присваивает параметры переменным своего класса.


class Project {
	var contractor: Company
	var name: String
	
	init?(name: String, company: Company) {
		guard !StateRegistry.hasCompany(name: company.name) else {
			return nil
		}
		self.contractor = company
		self.name = name
	}
}

var set: Set<Company> = []

set.insert(Company())
set.insert(Company())
set.insert(Company())

print(set)



// 7. После имплементации всего вышеуказанного, необходимо создать
// экземпляры компаний при помощи каждого инициализатора,
// "зарегистрировать" их в registeredCompanies, и создать инстансы класса
// Project.

let employee1 = Employee(firstName: "Employee", lastName: "One")
let employee2 = Employee(firstName: "Employee", lastName: "Two")
let employee3 = Employee(firstName: "Employee", lastName: "Three")



let weirdCompany = Company()
let failedCompany = Company(employee: nil, name: "Failed Company")
let failedFoodCompany1 = FoodCompany(employee: ("Employee", "Five"), name: "", qualityCertificate: "")
let failedFoodCompany2 = FoodCompany(employee: ("", ""), name: "Failed food company two", qualityCertificate: "OK")



let company1 = Company(list: [employee1, employee2], name: "Company 1")
StateRegistry.registerCompany(company1)
let company1Project = Project(name: "Project 1", company: company1)
print("company1Project", company1Project)


if let company2 = Company(employee: employee3, name: "Company 2") {
	StateRegistry.registerCompany(company2)
	let company2Project = Project(name: "Project 2", company: company2)
	print("company2Project", company2Project)

}

if let foodCompany = FoodCompany(employee: ("Employee", "Four"), name: "Food Company", qualityCertificate: "OK") {
	StateRegistry.registerCompany(foodCompany)
	let foodCompanyProject = Project(name: "Project 3", company: foodCompany)
	print("foodCompany", foodCompanyProject)
}

let unregisteredCompany = Company(list: [employee1, employee2], name: "Unregistered Company")
let unregisteredCompanyProject = Project(name: "Project 4", company: unregisteredCompany)
print("unregisteredCompanyProject", unregisteredCompanyProject)
